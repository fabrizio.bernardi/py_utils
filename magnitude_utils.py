import os
import sys
import math
import numpy as np
from obspy.geodetics.base import calc_vincenty_inverse
from obspy.geodetics import kilometers2degrees


def get_distance_min_mean(lat, lon):

    icount = 0
    distance = 0.0
    distanceMinMean = 0.0

    for i in range(len(lat)):

        distanceMin = sys.float_info.max

        for j in range(len(lat)):

            if (i != j):
                # Return distance in meters
                (distance, az, baz) = calc_vincenty_inverse(lat[i], lon[i], lat[j], lon[j])
                distance = kilometers2degrees(distance / 1000.)

                if (distance < distanceMin):
                   distanceMin = distance
                   
                           
        distanceMinMean = distanceMinMean + distanceMin
        icount = icount + 1
        

    if(icount > 0):
         distanceMinMean = distanceMinMean / float(icount)

    return distanceMinMean

def get_distance_max_mean(lat, lon):

    icount = 0
    distance = 0.0
    distanceMaxMean = 0.0

    for i in range(len(lat)):

        distanceMax = sys.float_info.min

        for j in range(len(lat)):

            if (i != j):
               (distance, az, baz) = calc_vincenty_inverse(lat[i], lon[i], lat[j], lon[j])
               distance = kilometers2degrees(distance /1000.)

               if (distance > distanceMax):
                   distanceMax = distance
        
        distanceMaxMean = distanceMaxMean + distanceMax
        icount = icount + 1

    if(icount > 0):
        distanceMaxMean = distanceMaxMean / float(icount)

    return distanceMaxMean

def get_distribution_weights(lat, lon):

    # counters: 
    distance    = 0.0
    weight      = 0.0
    area        = 0.0
    areaMinMean = 0.0
    weightSum   = 0.0
    weights     = np.zeros(len(lat))

    # find min distances
    distanceMinMean = get_distance_min_mean(lat, lon)
    areaMinMean = distanceMinMean * distanceMinMean
    
    for i in range(len(lat)):

        Sum = 1.0

        for j in range(len(lat)):

        	if (i != j):
                   (distance, az, baz) = calc_vincenty_inverse(lat[i], lon[i], lat[j], lon[j])
                   distance = kilometers2degrees(distance /1000.)
                   area = distance * distance

                   if(area < areaMinMean):

                       Sum = Sum + 1 - (area / areaMinMean)
                       

        weight     = 1.0 / Sum
        weights[i] = weight
        weightSum  = weightSum + weight
        
    weights = weights/weightSum

    return weights
