#!/usr/bin/env python

########### Script to update station list and instrument response  ##############


import os
import sys
import time
import errno
import argparse
import numpy as np
import matplotlib.pyplot as plt
from obspy import Stream
from obspy import UTCDateTime
from obspy import read_inventory
from obspy.signal.filter import lowpass
#obspy.signal.filter.lowpass
from obspy.signal.filter import highpass
from obspy.signal.filter import bandpass
from time import gmtime, strftime
from obspy.clients.fdsn import Client
#from mpl_toolkits.basemap import Basemap
#from obspy.geodetics import gps2DistAzimuth
from obspy.geodetics import locations2degrees
from obspy.geodetics.base import gps2dist_azimuth

def parseMyLine():

    ll = sys.argv[1:]
    if not ll:
       print ('Data mining script via fdsn. \nType ' + sys.argv[0] + ' -h or --help for guide and examples')
       sys.exit(0)

    Description =  'Data mining script via fdsn.\n\nLook for station data and metadata in geographic specified\n' \
                   'regions. The script inquire for data and metadata to all hosts (repository) that provide\n' \
                   'such service for all network available.\n' \
                   'i. Only one channel (BH* / LH* / etc s allowed\n' \
                   'ii. be sure to update the host lists. See obspy-fdsn spec\n' \
                   'iii. contact: fabrizio.bernardi@ingv.it\n'

    examples = 'EXAMPLES:\n' \
               '=========\n\n' \
               'Download waveform (BH*) and metadata for all networks and hosts in the Mediterranean region (default):\n\n' \
               '' + sys.argv[0] + ' --beg 2003-12-11T12:23:00 --len 100\n' \
               '\n' \
               'Download only metadata (BH*) for all networks and hosts in the Mediterranean region (default):\n\n' \
               '' + sys.argv[0] + ' --beg 2003-12-11T12:23:00 --len 100 --wave N\n' \
               '\n' \
               'Download only waveform (LH*) for MN network and IRIS and INGV host in the Mediterranean region (default):\n\n' \
               '' + sys.argv[0] + ' --beg 2003-12-11T12:23:00 --len 100 --host "IRIS INGV" --net MN --cha LH* --paz N --xml N\n' \
               '\n' \
               'Download waveform and metadata for all networks and hosts in an specific region from epicenter (method 1):\n\n' \
               '' + sys.argv[0] + ' --beg 2003-12-11T12:23:00 --len 100 --epi "23 45" --ddeg 15\n' \
               '\n' \
               'Download waveform and metadata for all networks and hosts in an specific region rectangular (method 2):\n\n' \
               '' + sys.argv[0] + ' --beg 2003-12-11T12:23:00 --len 100 --lat_min 10 --lat_max 20 --lon_min 0 --lon_max 30\n' \
               '\n' \
               'Download waveform from 10 minutes ago till now:\n\n' \
               '' + sys.argv[0] + ' --beg -1 --pre 600 for all networks and hosts in the Mediterranean regionn' 



    parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter,
                                     description=Description, epilog=examples)

    parser.add_argument('--host', default='*', help='fdsn host [BRG]. Default: *')
    parser.add_argument('--beg', default='-1', help='Begin of the time window ("2003-12-11T12:23:00"). [1 week ago]')
    parser.add_argument('--pre', default='0', help='pre time before begin time window')
    parser.add_argument('--len', default='100', help='length of sencond after begin time window [100] s')
    parser.add_argument('--net', default='*', help='networks [*]')
    parser.add_argument('--cha', default='BH*', help='channel [BH*]. Only one item allowed')
    parser.add_argument('--epi', default='None', help='Epicenter Coordinate [None]')
    parser.add_argument('--ddeg', default='10', help='Distance from epicenter for request corner area [10]')
    parser.add_argument('--lat_min', default='20', help='Min latitude [20.0]')
    parser.add_argument('--lat_max', default='60', help='Max latitude [60.0]')
    parser.add_argument('--lon_min', default='-20', help='Min longitude [-20.0]')
    parser.add_argument('--lon_max', default='50', help='Max longitude [50.0]')
    parser.add_argument('--datalesslike', default='N', help='Instrument response output in sac PAz file format \
                                                             as extracted using rdseed Y/[N]')
    parser.add_argument('--namemseed', default='waveform.mseed', help='mseed file name for waveformes [data.mseed]')
    parser.add_argument('--format_mseed', default='Y', help='Save waveforms into mseed (one file with all traces). [Y]/N')
    parser.add_argument('--format_sac', default='Y', help='Save waveforms into sac (one file for each trace). [Y]/N')
    parser.add_argument('--reclen', default=512, help='mseed record length. See obspy spec. for details. [512]')
    parser.add_argument('--encoding', default='FLOAT64', help='mseed encoding. See obspy spec. for details. [FLOAT64]')
    parser.add_argument('--mapstep', default='10', help='longitude and latitude step intervals for map [10] degree.')
    parser.add_argument('--kml', default='stations_file.kml', help='kml file format with station coordinate(Sta lat lon) [stations_file.kml]')
    parser.add_argument('--StsFile', default='stations_file.txt', help='Station coordinate file (Sta lat lon ele) [stations_file.txt]')
    parser.add_argument('--StsFileEx', default='stations_file_extended.txt', help='Extended station coordinate file (host net sta cha lat lon ele) [stations_file_extended.txt]/None')
    parser.add_argument('--map', default='map', help='map file name source with downloaded data (map and waves)[map]')
    parser.add_argument('--paz', default='Y', help='Instrument response output in sac PAz file format Y/[N]')
    parser.add_argument('--xml', default='Y', help='Instrument response output in xml file format [Y]/N')
    parser.add_argument('--wave', default='Y', help='want to download waveforms [Y]/N')
    parser.add_argument('--log', default='Summary.log', help=' Log file [Summary.log]')
    parser.add_argument('--pazpath', default='paz', help='polzero files subdirectory. [paz]')
    parser.add_argument('--xmlpath', default='xml', help='resp files subdirectory. [resp]')
    parser.add_argument('--wavepath', default='wave', help='waveforms subdirectory. [wave]')
    parser.add_argument('--logpath', default='log', help='logs subdirectory. [log]')
    parser.add_argument('--attach_response', default=False, help='Attach resonse to the waveforms [True]/False')
    parser.add_argument('--bandpass', default="N", help='bandpass filter [N]/Y')
    parser.add_argument('--highpass', default="N", help='highpass filter [N]/Y')
    parser.add_argument('--lowpass', default="N", help='lowpass filter [N]/Y')
    parser.add_argument('--freqmin', default="0.01", help='Min frequency for bandpass filter [0.01]')
    parser.add_argument('--freqmax', default="0.02", help='Max frequency for bandpass filter [0.02]')
    parser.add_argument('--freq', default="0.1", help='frequency corner for high/lowpass [0.1]')
    parser.add_argument('--corners', default=2, help='Filters corner order [2]')
    parser.add_argument('--filterto', default="None", help='Apply filter to plot (p), to waves (w) or both (pw). [None]')

    args=parser.parse_args()

    if(args.xml == 'N' and args.paz == 'N'):
       print("Please select one or more instrument response output file format")

    if(args.host == '*'):
       args.host = ['INGV', 'IRIS'] 
       args.host = ['BGR','ETH' 'GEONET', 'GFZ', 'INGV', 'IPGP', 'IRIS', 'KOERI', \
                    'NCEDC', 'LMU', 'NCEDC', 'NEIP', 'NERIES', 'ORFEUS', 'RESIF', \
                    'SCEiDC', 'USGS', 'USP'] 
    else:
       foe = args.host.split(' ')
       tmp = []
       for i in range(len(foe)):
           tmp.append(foe[i])
       args.host = tmp
   
    if(args.net == '*'):
       pass
    else:
       foe = args.net.split(' ')
       tmp = []
       for i in range(len(foe)):
           tmp.append(foe[i])
       args.net = tmp

    return args

def selectnet(net, args):

    if (args.net != '*'):
       out = []
       for i in range(len(args.net)):
           if args.net[i] in net:
              out.append(args.net[i])  

       return out

    else:
       return net

def inv2networks(inv):

    lista = []
    for a, v in inv.get_contents().items():
        if(a == 'networks'):
           lista = v

    return lista

def inv2stations(inv):

    lista = []
    for a, v in inv.get_contents().items():
        if(a == 'stations'):
           lista = v

    for i in range(len(lista)):
        tmp = lista[i].split()
        lista[i] = tmp[0] 

    return lista

def make_sure_path_exists(args):

    if(args.wave == 'Y'):
       try:
           os.makedirs(args.wavepath)
       except OSError as exception:
           if exception.errno != errno.EEXIST:
              raise

    if(args.xml == 'Y'):
       try:
           os.makedirs(args.xmlpath)
       except OSError as exception:
           if exception.errno != errno.EEXIST:
              raise

    if(args.paz == 'Y'):
       try:
           os.makedirs(args.pazpath)
       except OSError as exception:
           if exception.errno != errno.EEXIST:
              raise

    # create log directory
    try:
       os.makedirs(args.logpath)
    except OSError as exception:
       if exception.errno != errno.EEXIST:
          raise

def create_StsFile(args):

    fo = 1
    if (args.StsFile != "None"):
       f = args.StsFile.split(os.sep)
       if len(f) > 1:
          last = f.pop()
          n = os.sep.join(f)    
          make_sure_path_exists(args)

       fo = open(args.StsFile, "w")

    ex = 1
    if (args.StsFileEx != "None"):
       f = args.StsFileEx.split(os.sep)
       if len(f) > 1:
          last = f.pop()
          n = os.sep.join(f)    
          make_sure_path_exists(args)

       ex = open(args.StsFileEx, "w")



    log = open(args.logpath + os.sep + args.log, "w")

    return fo, ex, log


def stsfrominv(inv):

    for network in inv:
          for station in network:
              string = ("%5d %2d %8.4f %9.4f %6.1f\n") % (station.code, network.code,\
                       station.latitude, station.longitude, station.elevation)
              print(network, station, string)
    return string


def CenterToCorner(args):

    if(args.epi != "None"):
       foe = args.epi.split()
       lat = float(foe[0])
       lon = float(foe[1])
       deg = float(args.ddeg)

       args.lat_min = lat - deg
       args.lat_max = lat + deg
       if(args.lat_min < -89.0):
          args.lat_min = -89.0
       if(args.lat_max > +89.0):
          args.lat_max = +89.0

       args.lon_min = lon - deg
       args.lon_max = lon + deg
       if(args.lon_max >= 180):
          args.lon_max = 180 - args.lon_max 
       if(args.lon_min <= -180):
          args.lon_min = 180 - args.lon_min 
       

    args.lon_min = "%.3f" % (float(args.lon_min))
    args.lon_max = "%.3f" % (float(args.lon_max))
    args.lat_min = "%.3f" % (float(args.lat_min))
    args.lat_max = "%.3f" % (float(args.lat_max))

    return args

def set_Times(args):

   if(args.beg == '-1'):
      # here if you desire real time
      now           = strftime("%Y-%m-%d %H:%M:%S", gmtime())
      nowUTC        = UTCDateTime(now)
      start_time    = UTCDateTime(nowUTC - float(args.pre))
      end_time      = UTCDateTime(nowUTC) 
   else:
      start_time    = UTCDateTime(args.beg) - float(args.pre)
      end_time      = start_time + float(args.len)

   return start_time, end_time

def placeHostsandNets(Hosts, Nets, host, lista):

    if (len(lista) > 0):
    
       for i in range(len(lista)):

           Hosts.append(host)
           Nets.append(lista[i])

    return Hosts, Nets

def selectOnlyOneCouple(Hosts, Nets):

    selected_hosts = []

    # get unique networks
    selected_nets = list(set(Nets)) 

    # for each intem in net get index in Nets, and select the first
    for i in range(len(selected_nets)):
        index = Nets.index(selected_nets[i])
        selected_hosts.append(Hosts[index])

    return selected_hosts, selected_nets

def selectCompletenessWaves(st, beg, end):
 
    complete = Stream()

    for i in range(len(st)):
        npts_teo = (end-beg)/float(st[i].stats.delta)
        npts_obs = float(st[i].stats.npts)
        if (int(npts_obs) >= int(npts_teo)):
           complete.append(st[i])

    return complete

def selectUniqueWaves(st):

    elements = []
    unique = Stream()

    for i in range(len(st)):
        lis = st[i].stats.network + "." + st[i].stats.station
        elements.append(lis)

    singles = list(set(elements))

    for i in range(len(singles)):
        b = [item for item in range(len(elements)) if elements[item] == singles[i]]
        unique.append(st[b[0]])
        unique.append(st[b[1]])
        unique.append(st[b[2]])
    
    return unique

def setPlotStatus(meta, wave):

    typ=[]
    for i in range(len(meta)):
        status = any(meta[i] in s for s in wave)
        if (status == True):
           typ.append(1)
        else:
           typ.append(2)

    return typ

def plotMap(plot_status, sta, lat, lon, args):

    f = args.epi.split()

    m = Basemap(llcrnrlon=float(args.lon_min), llcrnrlat=float(args.lat_min), \
                urcrnrlon=float(args.lon_max), urcrnrlat=float(args.lat_max), \
                projection='merc', resolution ='l')
    m.drawcoastlines()
    m.drawcountries()
    m.fillcontinents(color='#E5E4E2',lake_color='white')
    m.drawparallels(np.arange(float(args.lat_min), float(args.lat_max), float(args.mapstep)),labels=[1,0,0,0], fontsize=6)
    m.drawmeridians(np.arange(float(args.lon_min), float(args.lon_max), float(args.mapstep)),labels=[0,0,0,1], fontsize=6)

    for i in range(len(sta)):
        if plot_status[i] == 1:
           plot_fig = 'o'
           plot_size = 5
           plot_color = 'blue'
        else:
           plot_fig = 's'
           plot_size = 4
           plot_color = 'red'
        x, y = m(float(lon[i]), float(lat[i]))
        m.plot(x, y, plot_fig, markersize=plot_size, color=plot_color)

    if(args.epi != "None"):
       x, y = m(float(f[1]), float(f[0]))
       m.plot(x, y, 'D', markersize=6, color='y') 

    #plot epicenter

    plt.savefig(args.map + 'geographic.pdf')
    
    return True

def applyFilter(st, args):

    spin = 0

    if (args.bandpass == "Y"):

        st.detrend('demean')
        st.detrend('linear')
        st.taper(max_percentage=0.01)

        st.filter('bandpass', freqmin=float(args.freqmin), \
                              freqmax=float(args.freqmax), \
                              corners=int(args.corners))
        spin = 1

    if (args.lowpass != "Y" and spin == 0):

        st.detrend('demean')
        st.detrend('linear')
        st.taper(max_percentage=0.01)

        st.filter('lowpass', freq=float(args.freq), corners=int(args.corners))
        spin = 1

    if (args.highpass != "Y" and spin == 0):

        st.detrend('demean')
        st.detrend('linear')
        st.taper(max_percentage=0.01)

        st.filter('lowpass', freq=float(args.freq), corners=int(args.corners))
        spin = 1

    return st

####################################################################
########                     BEGIN                          ########
####################################################################


# 1: Read arguments 
# ==================================================================
args=parseMyLine()


# 2: Set statrt and end time
# ==================================================================
start_time, end_time = set_Times(args)


# 3: Set area
# ==================================================================
args=CenterToCorner(args)


# 4: Set outdir to save data (waveforms and/or instrument response)
make_sure_path_exists(args)
# ==================================================================


# 5. Make stationfile (to put inside CODE NET LAT LON ELE), and log
# ==================================================================
StsFile, StsFileEx, log = create_StsFile(args)


#===================================================================
# A: MAKE LOOP OVER HOSTS TO FIND NETWOKS AVAILABLE
#    For each host check which networks are available. The idea is
#    that a host has instrument response available vis fdsn, the
#    same host should also be able to provide data for that network
#    (data may also be not available because of timewindw)
#===================================================================


# A1: For each host should resut a network
#     Example ['IRIS', 'INGV', 'INGV']
#             ['MN', 'MN', 'IV']
# ==================================================================
available_hosts = []
available_nets  = []


# A2: Begin loop over hosts
# ==================================================================
for i in range(len(args.host)):

    # Initialize client
    try:
       client = Client(args.host[i])
    except:
       print ("No connection with host " + args.host[i])
       pass

    # Get netwoks 
    try:
       inv = client.get_stations(network="*", 
                                 station="*", 
                                 channel=args.cha, 
                                 level="network", 
                                 starttime=start_time, 
                                 endtime=end_time, 
                                 minlatitude=float(args.lat_min),
                                 maxlatitude=float(args.lat_max),
                                 minlongitude=float(args.lon_min),
                                 maxlongitude=float(args.lon_max))
    except:
       print ("No network available for that time window on host " + args.host[i])
       pass

    # Get list of networks
    try:
       networks_list = inv2networks(inv)
    except:
       print ("No connection available for " + args.host[i])
       sys.exit()

#   # Refine list of networks: if args.net != *, select args.net form networks_list
    try:
       networks_list = selectnet(networks_list, args)
       # Resulting Network lists
       print("Networks available in host " + args.host[i] + ": ",networks_list)

       # put host and networks in their own lists
       available_hosts, available_nets = placeHostsandNets(available_hosts, available_nets, args.host[i], networks_list)

    except:
       print ("No Networks available in host " + args.host[i] + ": ",networks_list)
       pass


# A3: Select only one couple of host-network for each network available. See A1 for comparison
#     Example ['IRIS', 'INGV']    
#             ['MN', 'IV']
# ==================================================================
#available_hosts, available_nets = selectOnlyOneCouple(available_hosts, available_nets)
print ("")
print ("SELECTED HOSTS    : " + str(available_hosts))
print ("AVAILABLE NETWORKS: " + str(available_nets))


#===================================================================
# B: MAKE LOOP OVER SELECTED HOSTS-NETWOKS TO MINING ALL STSTION
#    LEVEL AVAILABLE. 
#    i.e.: available_hosts, available_nets
#    RETURN COMBINATION FOR ALL HOST.NETWOK.STATION 
#===================================================================


# B1: Initialize lists and stream
#===================================================================
all_host_net_stations_list = []
strings_station = []
waveforms = Stream()


# B2: Loop over hosts-networks 
#===================================================================
for i in range(len(available_nets)):

    # Initialize client
    try:
       client = Client(available_hosts[i])
    except:
       print ("No connection with host " + available_hosts[i])
       pass


    # B3: Get list of station available for that network at that host
    #===================================================================
    try:
       inv = client.get_stations(network=available_nets[i], 
                                 station="*", 
                                 channel=args.cha, 
                                 level="station",
                                 starttime=start_time, 
                                 endtime=end_time, 
                                 minlatitude=float(args.lat_min), 
                                 maxlatitude=float(args.lat_max), 
                                 minlongitude=float(args.lon_min), 
                                 maxlongitude=float(args.lon_max))

       # get station list
       stations_list = inv2stations(inv)
       print ("AVAILABLE ON HOST ", available_hosts[i],": ",stations_list)

       # B4: Merge all host-network-station 
       #===================================================================
       for j in range(len(stations_list)):
           all_host_net_stations_list.append(available_hosts[i] + "." + stations_list[j])

    except:
       print ("No Station informations from host " +  available_hosts[i] + " for network " + available_nets[i])


#print ("AVAILABLE STATIONS: ", all_host_net_stations_list)



# C: FOR ALL HOST.NETWOK.STATION IF ARGS.WAVE == Y, USE 
#    get_waveforms_bulk(bulk)
#    DOWNLOAD ALLA AVAILABLE DATA COMBINATION, THEN 
#    SELECT COMPLETE AND UNIQUE
#    NOW IN ORDER TO AVOID DOUBLE METADATADOWNLOAD: AS IN FIRST HOST 
#    DOWNLOADED, STORE STATION. IN NEXT HIST CHECH STSTION
#===================================================================
if (args.wave == "Y"):

   print ("")
   print ("BEGIN WAVEFORM DOWNLOAD")   

   # C1: In order to have the minimum initialization of the client
   #     starts with loop over host. Skip all_host_net_stations_list 
   #     with unequal host
   #===================================================================
   waveform_stations = []
   all_host_net_stations_bulk_true = []
   all_stations_wf_4_plot = []
   downloaded_all_host_net_stations_list = []
   waves = Stream()

   # get unique networks
   available_hosts = list(set(available_hosts))

   for i in range(len(available_hosts)):

     try:
       client = Client(available_hosts[i])

       for n in range(len(all_host_net_stations_list)):

           f  = all_host_net_stations_list[n].split('.')
           f_host = f[0]
           f_netw = f[1]
           f_staz = f[2]

           # pass is station already in waveform_stations
           status = any(f_staz in s for s in waveform_stations)

           if (f_host == available_hosts[i] and status == False):
            
              bulk = [(f_netw, f_staz, "*", args.cha, start_time, end_time)]

              print ("Download waveform:", all_host_net_stations_list[n])
                    
              try:
                 st = client.get_waveforms_bulk(bulk, attach_response=args.attach_response)
                 log.write("OK data for " + all_host_net_stations_list[n] + " -- " + str(bulk) + "\n")
                
                 waveform_stations.append(f_staz)

                 all_stations_wf_4_plot.append(st[0].stats.station)

                 downloaded_all_host_net_stations_list.append(all_host_net_stations_list[n])

                 print ("................................ OK")

                 for j in range(len(st)):
                     st[j].stats.host = available_hosts[i]
                     waves.append(st[j])
              except:
                 log.write("No data for " + all_host_net_stations_list[n] + " -- " + str(bulk) + "\n")
                 pass
        
     except:
       pass
           

   # C2: Select only complete data (npts = (tb-te)/delta)
   #===================================================================
   waves = selectCompletenessWaves(waves, start_time, end_time)

   # C3: Apply filter if required with --filterto w
   if (args.filterto == "w" or args.filterto == "wp" or args.filterto == "pw"):
      waves = applyFilter(waves, args)


   # C3: Save data in sac o mseed file format 
   #===================================================================
   if (args.format_mseed == 'Y'):
      if(args.encoding=='FLOAT64'):
           for i in range(len(waves)):
               waves[i].data = waves[i].data * [1.0] 

      waves.write(args.wavepath + os.sep + args.namemseed, format="MSEED",
                  reclen=int(args.reclen),  encoding=args.encoding) 

   if(args.format_sac == 'Y'):
      for i in range(len(waves)):
          if(args.datalesslike == "N"):
             name = waves[i].stats.station + "." + waves[i].stats.network + "." + waves[i].stats.host + "." + waves[i].stats.channel + ".sac"
          else:
             u_time = str(waves[i].stats.starttime)
             name = u_time.replace('Z','').replace(':','.') + "." + waves[i].stats.network + "." + waves[i].stats.station + ".." + waves[i].stats.channel + ".M.SAC"
          waves[i].write(args.wavepath + os.sep + name, format="SAC")



# D: GET METADATA 
#    NOW IN ORDER TO AVOID DOUBLE METADATADOWNLOAD: AS IN FIRST HOST 
#    DOWNLOADED, STORE STATION. IN NEXT HIST CHECH STSTION
#===================================================================
print ("")
if (args.xml == "Y" or args.paz == "Y"):
  print ("BEGIN METADATA DOWNLOAD")   
  print (downloaded_all_host_net_stations_list)
  if(len(downloaded_all_host_net_stations_list) >=1):


   metadata_stations = []
   matadata_latitude = []
   matadata_longitude = []
   matadata_distance = []
   all_stations_met_4_plot = []
#  kml=simplekml.Kml()
  
   cZ = args.cha[0:2] + 'Z'
   cN = args.cha[0:2] + 'N'
   cE = args.cha[0:2] + 'E'
   c0 = args.cha[0:2] + '0'
   c1 = args.cha[0:2] + '1'
   c2 = args.cha[0:2] + '2'


   for i in range(len(downloaded_all_host_net_stations_list)):

           f  = downloaded_all_host_net_stations_list[i].split('.')
           f_host = f[0]
           f_netw = f[1]
           f_staz = f[2]

           client = Client(f_host)

#      for n in range(len(downloaded_all_host_net_stations_list)):


           # pass is station already in metadata_stations
           status = any(f_staz in s for s in metadata_stations)

#          if (f_host == available_hosts[i] and status == False):
           if (status == False):

              tmp = downloaded_all_host_net_stations_list[i].split('.')
              spin = 0
   

              if(args.xml == 'Y'):
                 newXMLZ = args.xmlpath + os.sep + "resp." + tmp[1] + "." + tmp[2] + "." + cZ + ".xml"
                 newXMLN = args.xmlpath + os.sep + "resp." + tmp[1] + "." + tmp[2] + "." + cN + ".xml"
                 newXMLE = args.xmlpath + os.sep + "resp." + tmp[1] + "." + tmp[2] + "." + cE + ".xml"
                 newXML0 = args.xmlpath + os.sep + "resp." + tmp[1] + "." + tmp[2] + "." + c0 + ".xml"
                 newXML1 = args.xmlpath + os.sep + "resp." + tmp[1] + "." + tmp[2] + "." + c1 + ".xml"
                 newXML2 = args.xmlpath + os.sep + "resp." + tmp[1] + "." + tmp[2] + "." + c2 + ".xml"
              if(args.paz == 'Y'):
                 if(args.datalesslike == 'Y'):
                    #SAC_PZs_IV_VAGA_HHN__2012.232.17.41.54.2600_2012.232.17.52.04.7000
                    newpazZ = args.pazpath + os.sep + "SAC_PZs_" + tmp[1] + "_" + tmp[2] + "_" + cZ + "__data"
                    newpazN = args.pazpath + os.sep + "SAC_PZs_" + tmp[1] + "_" + tmp[2] + "_" + cN + "__data"
                    newpazE = args.pazpath + os.sep + "SAC_PZs_" + tmp[1] + "_" + tmp[2] + "_" + cE + "__data"
                    newpaz0 = args.pazpath + os.sep + "SAC_PZs_" + tmp[1] + "_" + tmp[2] + "_" + c0 + "__data"
                    newpaz1 = args.pazpath + os.sep + "SAC_PZs_" + tmp[1] + "_" + tmp[2] + "_" + c1 + "__data"
                    newpaz2 = args.pazpath + os.sep + "SAC_PZs_" + tmp[1] + "_" + tmp[2] + "_" + c2 + "__data"
                 else:
                    newpazZ = args.pazpath + os.sep + "resp." + tmp[1] + "." + tmp[2] + "." + cZ + ".pz"
                    newpazN = args.pazpath + os.sep + "resp." + tmp[1] + "." + tmp[2] + "." + cN + ".pz"
                    newpazE = args.pazpath + os.sep + "resp." + tmp[1] + "." + tmp[2] + "." + cE + ".pz"
                    newpaz0 = args.pazpath + os.sep + "resp." + tmp[1] + "." + tmp[2] + "." + c0 + ".pz"
                    newpaz1 = args.pazpath + os.sep + "resp." + tmp[1] + "." + tmp[2] + "." + c1 + ".pz"
                    newpaz2 = args.pazpath + os.sep + "resp." + tmp[1] + "." + tmp[2] + "." + c2 + ".pz"


              # Z Comp ----------------------
              try:
                 inv = client.get_stations(network=tmp[1],
                                           station=tmp[2],
                                           channel=cZ,
                                           level="response",
                                           starttime=start_time,
                                           endtime=end_time,
                                           minlatitude=float(args.lat_min),
                                           maxlatitude=float(args.lat_max),
                                           minlongitude=float(args.lon_min),
                                           maxlongitude=float(args.lon_max))

                 metadata_stations.append(tmp[2])

                 print ("Downloaded response:",f_host, newXMLZ)

                 if (spin == 0):
                   for network in inv:
                     for station in network:
                         string = "%-5s %8.4f %9.4f %7.1f\n" % (str(station.code),
                                                                float(station.latitude),
                                                                float(station.longitude),
                                                                float(station.elevation))

                         matadata_latitude.append(float(station.latitude))
                         matadata_longitude.append(float(station.longitude))

                         if (args.epi != "None"):
                            f = args.epi.split(' ')
                            azs      = gps2dist_azimuth(float(f[0]), float(f[1]), float(station.latitude), float(station.longitude))
                            Ddeg     = locations2degrees(float(f[0]), float(f[1]), float(station.latitude), float(station.longitude))
                            dkm      = "%d" % (azs[0])
                            matadata_distance.append(dkm)
                            stringEx = "%-7s %2s %-5s %2s %8.4f %9.4f %7.1f * %6.1f %6.1f %6.1f %6d\n" % (str(f_host),
                                                                                                     str(network.code),
                                                                                                     str(station.code),
                                                                                                     str(args.cha[0:2]),
                                                                                                     float(station.latitude),
                                                                                                     float(station.longitude),
                                                                                                     float(station.elevation),
                                                                                                     azs[1], azs[2], Ddeg, azs[0]/1000)
                            kml.newpoint(name='EPICENTER', coords=[(f[1], f[0])])
                            spin = 1
                         else:
                            stringEx = "%-7s %2s %-5s %2s %8.4f %9.4f %7.1f\n" % (str(f_host),
                                                                                  str(network.code), 
                                                                                  str(station.code),
                                                                                  str(args.cha[0:2]),
                                                                                  float(station.latitude),
                                                                                  float(station.longitude),
                                                                                  float(station.elevation))

                         kml.newpoint(name=str(station.code), coords=[(station.longitude, station.latitude)])
                         if (StsFile != 1):
                             StsFile.write(string)
                         if (StsFileEx != 1):
                             StsFileEx.write(stringEx)


              except:
                 print ("pipoooooooooo")
                 pass
              try:
                 inv.write(newXMLZ, format="STATIONXML")
              except:
                 pass
              try:
                 inv.write(newpazZ, format="SACPZ")
              except:
                 pass
         

              # N Comp ----------------------
              try:
                 inv = client.get_stations(network=tmp[1],
                                           station=tmp[2],
                                           channel=cN,
                                           level="response",
                                           starttime=start_time,
                                           endtime=end_time,
                                           minlatitude=float(args.lat_min),
                                           maxlatitude=float(args.lat_max),
                                           minlongitude=float(args.lon_min),
                                           maxlongitude=float(args.lon_max))

                 print ("Downloaded response:",f_host, newXMLN)

                 if (spin == 0):
                   for network in inv:
                     for station in network:
                         string = "%-5s %8.4f %9.4f %7.1f\n" % (str(station.code), 
                                                                float(station.latitude), 
                                                                float(station.longitude), 
                                                                float(station.elevation))

                         matadata_latitude.append(float(station.latitude))
                         matadata_longitude.append(float(station.longitude))

                         if (args.epi != "None"):
                            f = args.epi.split(' ')
                            azs      = gps2dist_azimuth(float(f[0]), float(f[1]), float(station.latitude), float(station.longitude))
                            Ddeg     = locations2degrees(float(f[0]), float(f[1]), float(station.latitude), float(station.longitude))
                            dkm      = "%d" % (azs[0])
                            matadata_distance.append(dkm)

                            kml.newpoint(name='EPICENTER', coords=[(f[1], f[0])])

                            spin = 1
 

              except:
                 pass
              try:
                 inv.write(newXMLN, format="STATIONXML")
              except:
                 pass
              try:
                 inv.write(newpazN, format="SACPZ")
              except:
                 pass

              # E Comp ----------------------
              try:
                 inv = client.get_stations(network=tmp[1],
                                           station=tmp[2],
                                           channel=cE,
                                           level="response",
                                           starttime=start_time,
                                           endtime=end_time,
                                           minlatitude=float(args.lat_min),
                                           maxlatitude=float(args.lat_max),
                                           minlongitude=float(args.lon_min),
                                           maxlongitude=float(args.lon_max))

                 print ("Downloaded response:",f_host, newXMLE)

                 if (spin == 0):
                   for network in inv:
                     for station in network:
                         string = "%-5s %8.4f %9.4f %7.1f\n" % (str(station.code), 
                                                                float(station.latitude), 
                                                                float(station.longitude), 
                                                                float(station.elevation))

                         matadata_latitude.append(float(station.latitude))
                         matadata_longitude.append(float(station.longitude))

                         if (args.epi != "None"):
                            f = args.epi.split(' ')
                            azs      = gps2dist_azimuth(float(f[0]), float(f[1]), float(station.latitude), float(station.longitude))
                            Ddeg     = locations2degrees(float(f[0]), float(f[1]), float(station.latitude), float(station.longitude))
                            dkm      = "%d" % (azs[0])
                            matadata_distance.append(dkm)

                            kml.newpoint(name='EPICENTER', coords=[(f[1], f[0])])

                            spin = 1
 
              except:
                 pass
              try:
                 inv.write(newXMLE, format="STATIONXML")
              except:
                 pass
              try:
                 inv.write(newpazE, format="SACPZ")
              except:
                 pass


#             # 0 Comp ----------------------
#             try:
#                inv = client.get_stations(network=tmp[1],
#                                          station=tmp[2],
#                                          channel=c0,
#                                          level="response",
#                                          starttime=start_time,
#                                          endtime=end_time,
#                                          minlatitude=float(args.lat_min),
#                                          maxlatitude=float(args.lat_max),
#                                          minlongitude=float(args.lon_min),
#                                          maxlongitude=float(args.lon_max))

#                print ("Downloaded response:",f_host, newXML0)

#                if (spin == 0):
#                  for network in inv:
#                    for station in network:
#                        string = "%-5s %8.4f %9.4f %7.1f\n" % (str(station.code), 
#                                                               float(station.latitude), 
#                                                               float(station.longitude), 
#                                                               float(station.elevation))

#                        matadata_latitude.append(float(station.latitude))
#                        matadata_longitude.append(float(station.longitude))

#                        if (args.epi != "None"):
#                           f = args.epi.split(' ')
#                           azs      = gps2dist_azimuth(float(f[0]), float(f[1]), float(station.latitude), float(station.longitude))
#                           Ddeg     = locations2degrees(float(f[0]), float(f[1]), float(station.latitude), float(station.longitude))
#                           dkm      = "%d" % (azs[0])
#                           matadata_distance.append(dkm)

#                           kml.newpoint(name='EPICENTER', coords=[(f[1], f[0])])

#                           spin = 1

#             except:
#                pass
#             try:
#                inv.write(newXML0, format="STATIONXML")
#             except:
#                pass
#             try:
#                inv.write(newpaz0, format="SACPZ")
#             except:
#                pass

#             # 1 Comp ----------------------
#             try:
#                inv = client.get_stations(network=tmp[1],
#                                          station=tmp[2],
#                                          channel=c1,
#                                          level="response",
#                                          starttime=start_time,
#                                          endtime=end_time,
#                                          minlatitude=float(args.lat_min),
#                                          maxlatitude=float(args.lat_max),
#                                          minlongitude=float(args.lon_min),
#                                          maxlongitude=float(args.lon_max))

#                print ("Downloaded response:",f_host, newXML1)

#                if (spin == 0):
#                  for network in inv:
#                    for station in network:
#                        string = "%-5s %8.4f %9.4f %7.1f\n" % (str(station.code), 
#                                                               float(station.latitude), 
#                                                               float(station.longitude), 
#                                                               float(station.elevation))

#                        matadata_latitude.append(float(station.latitude))
#                        matadata_longitude.append(float(station.longitude))

#                        if (args.epi != "None"):
#                           f = args.epi.split(' ')
#                           azs      = gps2dist_azimuth(float(f[0]), float(f[1]), float(station.latitude), float(station.longitude))
#                           Ddeg     = locations2degrees(float(f[0]), float(f[1]), float(station.latitude), float(station.longitude))
#                           dkm      = "%d" % (azs[0])
#                           matadata_distance.append(dkm)

#                           kml.newpoint(name='EPICENTER', coords=[(f[1], f[0])])

#                           spin = 1

#             except:
#                pass
#             try:
#                inv.write(newXML1, format="STATIONXML")
#             except:
#                pass
#             try:
#                inv.write(newpaz1, format="SACPZ")
#             except:
#                pass


#             # 2 Comp ----------------------
#             try:
#                inv = client.get_stations(network=tmp[1],
#                                          station=tmp[2],
#                                          channel=c2,
#                                          level="response",
#                                          starttime=start_time,
#                                          endtime=end_time,
#                                          minlatitude=float(args.lat_min),
#                                          maxlatitude=float(args.lat_max),
#                                          minlongitude=float(args.lon_min),
#                                          maxlongitude=float(args.lon_max))

#                print ("Downloaded response:",f_host, newXML2)

#                if (spin == 0):
#                  for network in inv:
#                    for station in network:
#                        string = "%-5s %8.4f %9.4f %7.1f\n" % (str(station.code), 
#                                                               float(station.latitude), 
#                                                               float(station.longitude), 
#                                                               float(station.elevation))

#                        matadata_latitude.append(float(station.latitude))
#                        matadata_longitude.append(float(station.longitude))

#                        if (args.epi != "None"):
#                           f = args.epi.split(' ')
#                           azs      = gps2dist_azimuth(float(f[0]), float(f[1]), float(station.latitude), float(station.longitude))
#                           Ddeg     = locations2degrees(float(f[0]), float(f[1]), float(station.latitude), float(station.longitude))
#                           dkm      = "%d" % (azs[0])
#                           matadata_distance.append(dkm)

#                           kml.newpoint(name='EPICENTER', coords=[(f[1], f[0])])

#                           spin = 1

#             except:
#                pass
#             try:
#                inv.write(newXML2, format="STATIONXML")
#             except:
#                pass
#             try:
#                inv.write(newpaz2, format="SACPZ")
#             except:
#                pass


   kml.save(args.kml)
   if (StsFile != 1):
    StsFile.close()
   if (StsFileEx != 1):
    StsFileEx.close()


# F: PLOT MAP, diamond only metadata, dots metadata and waveforms 
#===================================================================
plot_status = setPlotStatus(metadata_stations, all_stations_wf_4_plot)

#plot_map    = plotMap(plot_status, metadata_stations, matadata_latitude, matadata_longitude, args)

def updateWaves(st, sta, lat, lon, dist):

    Z = Stream()
    N = Stream()
    E = Stream()

    f = args.epi.split()

    for i in range(len(st)):
        for j in range(len(sta)):
            if (st[i].stats.station == sta[j]):
                st[i].stats.distance = dist[j]
                st[i].stats.st_lat   = lat[j]
                st[i].stats.st_lon   = lon[j]
                st[i].stats.evla   = f[0]
                st[i].stats.evlo   = f[1]
                st[i].stats.ev_coord = (float(f[0]), float(f[1]))
    for i in range(len(st)):
        if (st[i].stats.channel[-1] == "Z" or st[i].stats.channel[-1] == '0'):
           Z.append(st[i])
        if (st[i].stats.channel[-1] == "N" or st[i].stats.channel[-1] == '1'):
           N.append(st[i])
        if (st[i].stats.channel[-1] == "E" or st[i].stats.channel[-1] == '2'):
           E.append(st[i])

    return st, Z, N, E

def expandPlotnames(args):

    #f = args.map.split('.')
    #last = f.pop()
    #g = '.'.join(f)
    Z = args.map + 'waves_Z.pdf'
    N = args.map + 'waves_N.pdf'
    E = args.map + 'waves_E.pdf'
    
    return [Z, N, E]

plotnames = expandPlotnames(args)

waves, Z, N, E = updateWaves(waves, metadata_stations, matadata_latitude, matadata_longitude, matadata_distance)

if (args.filterto == "p"):
      waves = applyFilter(waves, args)

try:
   pltZ = Z.plot(type='section', handle=True, dist_degree=False)
except:
   pass
try:
   pltN = N.plot(type='section', handle=True, dist_degree=False)
except:
   pass
try:
   pltE = E.plot(type='section', handle=True, dist_degree=False)
except:
   pass

try:
   pltZ.savefig(plotnames[0])
except:
   pass
try:
   pltN.savefig(plotnames[1])
except:
   pass
try:
   pltE.savefig(plotnames[2])
except:
   pass
