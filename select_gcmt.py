#!/usr/bin/env python

import sys
import math
import argparse
import numpy as np
import cartopy.crs as ccrs
import matplotlib
import matplotlib.gridspec as gridspec
matplotlib.use("Agg")
import matplotlib.pyplot as pltlab
from obspy.core import UTCDateTime
import seaborn
#import plotting_defaults

from cartopy.mpl.gridliner import LONGITUDE_FORMATTER, LATITUDE_FORMATTER

def parseMyLine():

    ll = sys.argv[1:]
    if not ll:
       print ('GCMT solutions selector.')
       print ('Type: ' + sys.argv[0] + ' -h or --help for guide and examples')
       sys.exit(0)

    Description =  'Select events from GCMT ndk file based on date, magnitude and epicenter.' \
                   'List events in file and plot focal mechanism on map\n'

    examples = 'EXAMPLE (check defaults paramenters):\n' \
               '' + sys.argv[0] + ' --beg 2000-01-01T00:00:0 --end 2010-01-01T00:00:00 --min_mw 7.0\n'


    parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter,description=Description, epilog=examples)

    parser.add_argument('--min_lat', default='20',                 help='Min Lat. Default=20')
    parser.add_argument('--max_lat', default='50',                 help='Max Lat. Default=50')
    parser.add_argument('--min_lon', default='-10',                help='Min Lon. Default=-10')
    parser.add_argument('--max_lon', default='50',                 help='Max Lon. Default=50')
    parser.add_argument('--min_dep', default='0',                  help='Min Lon. Default=0')
    parser.add_argument('--max_dep', default='1000',               help='Max Lon. Default=1000')
    parser.add_argument('--min_mw',  default='5.4',                help='Min Mw. Default=4')
    parser.add_argument('--max_mw',  default='9.9',                help='Max Mw. Default=9.9')
    parser.add_argument('--beg',     default='1000-01-01T00:00:00',help='Begin Time. Default=2000-01-01T00:00:00')
    parser.add_argument('--end',     default='2019-12-31T23:00:00',help='End Time. Default=2015-10-10T00:00:00')
    parser.add_argument('--file',    default='gmt_selected_parameters.txt', \
                                                                   help='info file (date, epicenter and parameters) Default=gmt_selected_parameters.txt')
    parser.add_argument('--pdf',     default='gmt_selected_map.pdf',help='pdf map file name (gmt_selecte_map.pdf)')
    parser.add_argument('--ndk',     default='/Users/fabrizio/lib/gcmt/jan76_dec17.ndk', \
                                                                   help='gcmt_ndk libe file. Default=/Users/fabrizio/lib/gcmt/jan76_dec17.ndk')

    parser.add_argument('--symbol',  default='bbs',                help='Plot dots or bbs. Default=[bbs]/dot')
    parser.add_argument('--title',   default='GCMT selction',      help='Title of the plot')


    args=parser.parse_args()

    return args

def gcmt_list2gcmt_para(infile, args):

    min_lat = float(args.min_lat)
    max_lat = float(args.max_lat)
    min_lon = float(args.min_lon)
    max_lon = float(args.max_lon)
    min_dep = float(args.min_dep)
    max_dep = float(args.max_dep)
    min_mw  = float(args.min_mw)
    max_mw  = float(args.max_mw)
    min_t   = UTCDateTime(args.beg)
    max_t   = UTCDateTime(args.end)

    exx  = []
    idd  = []
    cid  = []
    dat  = []
    tim  = []
    lat  = []
    lon  = []
    dep  = []
    ara  = []
    mag  = []
    mom  = []
    st1  = []
    sl1  = []
    dp1  = []
    Mrr  = []
    Mtt  = []
    Mpp  = []
    Mrt  = []
    Mrp  = []
    Mtp  = []
    ndkList = []

    f = open(infile, 'r')
    for line in f:
        line = line.rstrip()
        ndkList.append(line)

    for i in range(0,len(ndkList)-0, 5):
        spin = 0
        a   = ndkList[i+0]
        b   = ndkList[i+3]
        c   = ndkList[i+4]
        d   = ndkList[i+1]
        e   = ndkList[i+2]
        eee = e.split()
        exp = str(b[0:2])
        exx.append(exp)
        sca = str(c[49:56])
        Mo  = float(sca + 'e' + exp)
        Mw  = math.log10(Mo)/1.5-10.73
        la  = float(a[27:33])
        lo  = float(a[34:41])
        de  = float(a[42:47])
        clat = float(eee[3])
        clon = float(eee[5])
        cdep = float(eee[7])
        ti  = str(a[5:15].replace('/','-')) + "T" + str(a[16:26])
        try:
          te  = UTCDateTime(ti)
        except:
          ts=ti.replace('60','59')
          te  = UTCDateTime(ts)

        if(clon <0):
            clon = 180 - clon

        de = cdep
        la = clat
        lo = clon


#        print(te, '--', cdep, de, '--', min_lat, la, max_lat, '--', min_lon, lo , max_lon, '--', min_mw, Mw, '--', min_dep, de)


        if(Mw >= min_mw and de <= min_dep and lo >= min_lon and lo <= max_lon and la >= min_lat and la <= max_lat):

          if(la >=30 and la <= 32):
               print('ciao')
          else:
#       if(Mw >= min_mw  and Mw <= max_mw and \
#           de >= min_dep and de <= max_dep and \
#           la >= min_lat and la <= max_lat and \
#           lo >= min_lon and lo <= max_lon and
#           te >= min_t and te <= max_t):
           idd.append(a[0:4])
           cid.append(d[0:14].replace(' ',''))
           dat.append(a[5:15])
           tim.append(a[16:26])
           lat.append(float(a[27:33]))
           lon.append(float(a[34:41]))
           dep.append(float(a[42:47]))
           ara.append(a[55:80])
           mij = b[2:80].split()
           Mrr.append(float(mij[0]))
           Mtt.append(float(mij[1]))
           Mpp.append(float(mij[2]))
           Mrt.append(float(mij[3]))
           Mrp.append(float(mij[4]))
           Mtp.append(float(mij[5]))
           npt = c[58:80].split()
           st1.append(float(npt[0]))
           sl1.append(float(npt[1]))
           dp1.append(float(npt[2]))
           mom.append("%.3e" % Mo)
           mag.append("%.2f" % (math.log10(Mo)/1.5-10.73))


    return idd, cid, dat, tim, lat, lon, dep, mag, mom, st1, sl1, dp1, Mrr, Mtt, Mpp, Mrt, Mrp, Mtp, exx, ara

#######################################################################################################

args=parseMyLine()

min_lat = float(args.min_lat)
max_lat = float(args.max_lat)
min_lon = float(args.min_lon)
max_lon = float(args.max_lon)
min_dep = float(args.min_dep)
max_dep = float(args.max_dep)
min_mw  = float(args.min_mw)
max_mw  = float(args.max_mw)

#gcmt_catalog = "/Users/fabrizio/lib/gcmt/jan76_dec13.ndk.txt"

eid, cid, date, time, lat, lon, depth, mw, mo, st1, sl1, dp1, Mrr, Mss, Mee, Mrs, Mre, Mse, exx, area = gcmt_list2gcmt_para(args.ndk, args)

focmecs = [0.136, -0.591, 0.455, -0.396, 0.046, -0.615]

f=open(args.file, 'w')
for i in range(len(eid)):
    date[i] = date[i].replace('/','-')
    l = "%-14s %sT%s %7.2f %7.2f %6.1f %3.1f %9.3e %6.1f %6.1f %6.1f %10.3e %10.3e %10.3e %10.3e %10.3e %10.3e %2d %s" % \
         (cid[i], date[i], time[i], float(lat[i]), float(lon[i]), float(depth[i]),\
          float(mw[i]), float(mo[i]), float(st1[i]), float(sl1[i]), float(dp1[i]),\
          float(Mrr[i]), float(Mss[i]), float(Mee[i]), float(Mrs[i]), float(Mre[i]),\
          float(Mse[i]), float(exx[i]), area[i])
    f.write(l + "\n")
f.close()

size_levelb  = 3
size_levelt  = 3

lat = np.array(lat)
lon = np.array(lon)

#print(lat)

gs  = gridspec.GridSpec(2, 3, width_ratios=[1, 1, 1], height_ratios=[1, 2])
pltlab.figure(1, figsize=(8.27, 11.69))
#pltlab.figure(1, figsize=(297,210))
ax1 = pltlab.subplot(gs[1,:2], projection=ccrs.PlateCarree(central_longitude=180))
ax1.coastlines(resolution='50m', linewidth=0.2, color='black')
ax1.plot(lon, lat, color='white', linewidth=0.01, markersize=size_levelb, marker='o', transform=ccrs.PlateCarree(), zorder=10, markeredgecolor='red')
pltlab.savefig(args.pdf, bbox_inches='tight')
