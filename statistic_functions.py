

import numpy as np


from scipy import stats
from scipy.stats.kde import gaussian_kde

def get_left_right(Mwp, mwp_mean):

    l = []
    r = []

    for i in range(len(Mwp)):
    	if (Mwp[i] < mwp_mean):
    		l.append(Mwp[i])
    	if (Mwp[i] > mwp_mean):
    		r.append(Mwp[i])

    pl = 100 * len(l) / len(Mwp)
    pr = 100 * len(r) / len(Mwp)


    return l, r, len(l), len(r), pl, pr


def statistic_1(Mwp):
	"""
	From a set of 1D data, compute 
	1) skeness
	2) the  kernel-density estimate using Gaussian kernels

	return some value and a dictionary 
	"""

    # Fist define skewness of the magnitude distribution
    skewness  = stats.skew(Mwp, axis=0, bias=True)

    # Find mean and std of the magnitude distribution
    mwp_mean = np.mean(Mwp)
    mwp_std  = np.std(Mwp)

    # get number of mwp values left or rigth of the distribution
    mwp_l, mwp_r, nr_left_mean, nr_right_mean, prc_left_mean, prc_right_mean = get_left_right(Mwp, mwp_mean)


    # Compute pdf
    density = gaussian_kde(Mwp)

    # Find Maximal magnitude
    mwp_sorted = np.sort(Mwp)
    pdf_sorted = density(np.sort(Mwp))
    pdf_max_ix = pdf_sorted.argmax()
    pdf_max_mw = mwp_sorted[pdf_max_ix]


    # Bandwidth
    bw  = density.covariance_factor()
    bwf = bw * mwp_std

    """
    print("\n")
    print("Statistics Log:")
    print("=================")
    print("Nr of samples         : %3d" % (len(Mwp)))
    print("Skewness              : %8.4f" % (skewness))
    print("Mean                  : %8.4f" % (mwp_mean))
    print("Std                   : %8.4f" % (mwp_std))
    print("Nr Mwp left           : %3d "  % (nr_left_mean))
    print("Nr Mwp right          : %3d "  % (nr_right_mean))
    print("%%  Mwp left           : %6.2f" % (prc_left_mean))
    print("%%  Mwp right          : %6.2f" % (prc_right_mean))
    print("Max_gaussian_kde(Mwp) : %9.5f" % (pdf_max_mw))
    print("\n")
    """

    dictionary = {'nr_of_sample'     : len(Mwp),
                  'Skewness'         : skewness,
                  'Mean'             : mwp_mean,
                  'Std'              : mwp_std,
                  'nr_mwp_left'      : nr_left_mean,
                  'nr_mwp_right'     : nr_right_mean,
                  'pc_mwp_left'      : prc_left_mean,
                  'pc_mwp_right'     : prc_right_mean,
                  'Max_gaussian_kde' : pdf_max_mw,
                  'bw'               : bw} 

    return(density, pdf_max_mw, pdf_max_ix, dictionary)
  