#!/usr/bin/env python

import os
import sys
import argparse
from obspy.core import read

def parseMyLine():

    ll = sys.argv[1:]
    if not ll:
       print ('GCMT solutions selector.')
       print ('Type: ' + sys.argv[0] + ' -h or --help for guide and examples')
       sys.exit(0)

    Description =  'List network stations and channel in a mseed file\n'

    examples = 'EXAMPLE (check defaults paramenters):\n' \
               '' + sys.argv[0] + ' --mseed pippo.mseed\n'


    parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter,description=Description, epilog=examples)

    parser.add_argument('--mseed', default=None,                 help='Min Lat. Default=20')

    args=parser.parse_args()

    return args


args=parseMyLine()

st = read(args.mseed)

for i in range(len(st)):
    print(st[i].stats.starttime, st[i].stats.endtime, st[i].stats.network, st[i].stats.station, st[i].stats.channel, st[i].stats.npts)
