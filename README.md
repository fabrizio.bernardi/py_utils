### selectgcmt.py 

#### GCMT solutions selector

    Type: ./select_gcmt.py -h or --help for guide and examples

Require a GCMT ndk file. Download it from http://www.globalcmt.org or download
and unzip the **gcmt_1976-01_2015_06.ndk.txt.gz** from this repository.
Use --ndk option in order to select the GCMT file.
 
tested: python 3.4


### data_mining_via_fdsn.py

#### Data mining script via fdsn

    Type ./data_mining_via_fdsn.py -h or --help for guide and examples

tested: python 3.4
