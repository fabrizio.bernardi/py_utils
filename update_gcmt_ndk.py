#! /usr/bin/env python

#import io
import os
import sys
import requests
import lxml.html

import obspy
#from obspy.core.event import read_events

import numpy as np


def get_gcmt(**kwargs):

    dom = kwargs.get('nkdlist',None)
    page = kwargs.get('page',None)

    out = dict()
    lat = []
    lon = []
    dep = []
    ort = []
    mag = []
    ndk = []

    #for i in range(0,20):
    for i in range(len(dom)):

        print('---', dom[i].text)

        try:
            cat = obspy.read_events(page + os.sep + dom[i].text)
        except:
            continue

        lat.append(cat[0].origins[0].latitude)
        lon.append(cat[0].origins[0].longitude)
        dep.append(cat[0].origins[0].depth)
        ort.append(cat[0].origins[0].time)
        mag.append(cat[0].magnitudes[0].mag)
        ndk.append(dom[i].text)


    out['lat'] = lat
    out['lon'] = lon
    out['dep'] = dep
    out['ort'] = ort
    out['mag'] = mag
    out['ndk'] = ndk

    return out

main_path = '/Users/fabrizio/cat/ee/ee_127-ee_119'
data_path = main_path + os.sep + 'data_ee127'
work_path = main_path + os.sep + 'work'
lib_path  = '/Users/fabrizio/lib/gcmt'

gcmtquick = 'https://www.ldeo.columbia.edu/~gcmt/projects/CMT/catalog/NEW_QUICK'



dom = lxml.html.fromstring(requests.get(gcmtquick).content)

gcmt = get_gcmt(page=gcmtquick, nkdlist=dom)

np.save(lib_path + os.sep + 'new_quick_gcmt.npy', gcmt, allow_pickle=True)
