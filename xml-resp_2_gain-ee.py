#!/usr/bin/env python

import os
import sys
import glob
import argparse
from obspy import read_inventory
from obspy import UTCDateTime

def parseMyLine():

    ll = sys.argv[1:]
    if not ll:
       print ("Type " + sys.argv[0] + " -h or --help for guide and examples")
       sys.exit(0)

    Description = "Xml instrument response to early_est gain file"

    examples    = "Example:\n" + sys.argv[0] + " --xml_path xml  --date \"2018-11-11T23:23:12.0\"\n"

    parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter,
                                     description=Description, epilog=examples)

    parser.add_argument('--xml_path',    default='.',     help='path to xml-response files, Default=pwd()')
    parser.add_argument('--out_path',    default='.',     help='path to store the gain file, Default=pwd()')
    parser.add_argument('--gain_file',   default='gain.csv', help='path to store the gain file, Default=gain.csv')
    parser.add_argument('--sts_file',    default='station_coordinates.csv', help='path to store the station_coordinates file, Default=station_coordinates.csv')
    parser.add_argument('--date',        default='now',      help='date for instrument parameters, Default=now (UTC)')

    args=parser.parse_args()

    return args


def xml2gainsvc(**kwargs):

    xml  = kwargs.get('xml', None)
    date = kwargs.get('date', None)
    args = kwargs.get('args', None)

    if (date=='now'):
        datetime=UTCDateTime.now()
    else:
        datetime=UTCDateTime(date)

    inv = read_inventory(xml)
    [net, sta, loc, cha] = inv.get_contents()['channels'][0].split('.')
    station = '.'.join([net, sta, loc, cha])
    net = inv[0]
    sta = net[0]
    cha = sta[0]
    nfa = inv.get_response(station,datetime).get_paz().normalization_factor
    gai = inv.get_response(station,datetime).get_paz().stage_gain
    nfr = inv.get_response(station,datetime).get_paz().normalization_frequency
    [net, sta, loc, cha] = inv.get_contents()['channels'][0].split('.')
    # boh, i set type=3. no idea of what it is....
    type_resp = 3
    if(loc == ''):
       loc = '--'


    return net, sta, loc, cha, datetime.year, datetime.julday, nfa, nfr, type_resp

def xml2stacoordinates(**kwargs):

    xml  = kwargs.get('xml', None)
    date = kwargs.get('date', None)
    args = kwargs.get('args', None)

    if (date=='now'):
        datetime=UTCDateTime.now()
    else:
        datetime=UTCDateTime(date)

    inv = read_inventory(xml)
    [network, station, loc, channel] = inv.get_contents()['channels'][0].split('.')
    net = inv[0]
    sta = net[0]
    cha = sta[0]

    return (network, station, cha.latitude, cha.longitude, cha.elevation, \
            datetime.year, datetime.month, datetime.day, channel, cha.azimuth, cha.dip)



args=parseMyLine()
list_xml = glob.glob(args.xml_path + os.sep + '*.xml')


f=open(args.out_path + os.sep + args.gain_file, 'w')
for i in range(len(list_xml)):
    a = xml2gainsvc(xml=list_xml[i], date=args.date, args=args)
    print(a, list_xml[i])
    out = ("%s %s %s %s %d %d %e %f %d\n" % (a))
    f.write(out)
f.close()

f=open(args.out_path + os.sep + args.sts_file, 'w')
for i in range(len(list_xml)):
    a = xml2stacoordinates(xml=list_xml[i], date=args.date, args=args)
    print(a, list_xml[i])
    out = ("%s %s %f %f %f %d %d %d -- %s %f %f \n" % (a))
    f.write(out)
f.close()
