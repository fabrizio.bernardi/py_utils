#!/usr/bin/env python



###!/opt/python/3.9.7/bin/python

import psutil
import sys
from psutil._common import bytes2human
import datetime
import contextlib
from contextlib import redirect_stdout



class prettyfloat(float):
    def __repr__(self):
        return "%5.1f" % self

def pprint_ntuple(nt):

    all = []
    for name in nt._fields:
        value = getattr(nt, name)
        if name != 'percent':
            value = bytes2human(value)
        s = print('%-10s : %7s' % (name.capitalize(), value))
        all.append(s)

    return s

def readable_mem(nt):

    vals = []
    nams = []
    for name in nt._fields:
        value = getattr(nt, name)
        if name != 'percent':
            value = bytes2human(value)
        nams.append(name)
        vals.append(value)

    return nams, vals


def main():
    #mport io
    #old_stdout = sys.stdout
    #sys.stdout = mystdout = io.StringIO()

    s = " "
    #sys.stdout = open('/data/fabrizio/logs/monitoring.log', 'w')

    print("%s %s %13s %45s %22s %30s" % ('Time', s, 'CPU usage', 'CPU percentage per cpu', 'Avarege load', 'Memory'), flush=True)
    print("=============================================================================================================================================================", flush=True)
       #f.close()
    while(True):
              now = datetime.datetime.now()
              #date_time = ("%04s-%02s-%02sT%02s:%02s.%02s" % (now.year, now.month, now.day, now.hour, now.minute, now.second))
              date_time = now.strftime("%Y-%m-%dT%H:%M:%S")
              cpu_syst, cpu_list, avg_load, mem_info = get_genaral_load()
              print("%s %5.1f  **  %5.1f %5.1f %5.1f %5.1f %5.1f %5.1f %5.1f %5.1f  **  %5.1f %5.1f %5.1f  **  %-5s = %5s  %-5s = %5s  %-5s = %5s  %-5s = %5s  %-5s = %5s" % \
                                                                                              (date_time, cpu_syst, cpu_list[0], cpu_list[1], cpu_list[2], cpu_list[3], \
                                                                                                  cpu_list[4], cpu_list[5], cpu_list[6], cpu_list[7], \
                                                                                               avg_load[0], avg_load[1], avg_load[2], \
                                                                                               mem_info[0][1][0:5], mem_info[1][1] ,\
                                                                                               mem_info[0][2][0:5], mem_info[1][2], mem_info[0][3][0:5], mem_info[1][3], \
                                                                                               mem_info[0][4][0:5], mem_info[1][4], mem_info[0][5][0:5], mem_info[1][5]), flush=True)




def get_cpu_usage_pct():
    """
    Obtains the system's average CPU load as measured over a period of 500 milliseconds.
    :returns: System CPU load as a percentage.
    :rtype: float
    """
    return psutil.cpu_percent(interval=0.5, percpu=True), psutil.cpu_percent(interval=0.5)

def get_genaral_load():
    """
    Obtains the system's average CPU load as measured over a period of 500 milliseconds.
    :returns: System CPU load as a percentage.
    :rtype: float
    """
    cpu_list = psutil.cpu_percent(interval=1.0, percpu=True)
    cpu_syst = psutil.cpu_percent(interval=1.0, percpu=False)
    cpu_nr   = psutil.cpu_count()

    avg_load = psutil.getloadavg()
    avg_load = [x / cpu_nr * 100 for x in avg_load]

    mem_info = psutil.virtual_memory()
    mem_info = readable_mem(mem_info)
    #print(mem_info[0][1], mem_info[1][1])

    return cpu_syst, cpu_list, avg_load, mem_info


    #sys.stdout.close()

if __name__ == "__main__":
   main()
